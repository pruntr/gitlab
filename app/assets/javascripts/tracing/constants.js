export const TIME_RANGE_FILTER_TOKEN_TYPE = 'time-range';
export const SERVICE_NAME_FILTER_TOKEN_TYPE = 'service-name';
export const OPERATION_FILTER_TOKEN_TYPE = 'operation';
export const TRACE_ID_FILTER_TOKEN_TYPE = 'trace-id';
export const DURATION_FILTER_TOKEN_TYPE = 'duration';
